﻿using System.Diagnostics;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.Json;

namespace KillProcess
{
    public class Program
    {

        //import in the declaration for GenerateConsoleCtrlEvent
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool GenerateConsoleCtrlEvent(ConsoleCtrlEvent sigevent, int dwProcessGroupId);
        public enum ConsoleCtrlEvent
        {
            CTRL_C = 0,
            CTRL_BREAK = 1,
            CTRL_CLOSE = 2,
            CTRL_LOGOFF = 5,
            CTRL_SHUTDOWN = 6
        }

        private const string FILEPATH = "config.json";
        static void Main(string[] args)
        {



            Console.WriteLine("В случае, если не передаётся не один параметр, то будет убит процесс dotnet с параметрами, которые указаны в файле config.json");
            Console.WriteLine("Если параметры переданы, то первым параметром должен идти путь к файлу проекта, а вторым номер позиции, которую надо заменить.");

            string pathFile = System.IO.Path.GetDirectoryName(Environment.ProcessPath);

            pathFile = Path.Combine(pathFile, FILEPATH);





            Console.WriteLine($"File: {pathFile}");
            if (!File.Exists(pathFile))
                Environment.ExitCode = -1;

            ConfigDTO config = JsonSerializer.Deserialize<ConfigDTO>(File.ReadAllText(pathFile));


            if (args.Length == 0)
            {
                Udalenie(config);
            }
            else
            {
                Versua(config, args);
            }
            File.WriteAllText(pathFile, JsonSerializer.Serialize(config));

        }


        private static void Udalenie(ConfigDTO config)
        {
            try
            {
                var localAll = Process.GetProcesses().ToList();

                Process proc = new Process();

                bool flag = false;
                foreach (Process process in localAll)
                {
                    try
                    {
                        ProcessModule? mm = process.MainModule;

                        var fileingo = mm.FileVersionInfo;

                        string temp = fileingo.FileName;
                        if (String.IsNullOrEmpty(temp)) continue;

                        if (temp.Contains("dotnet.exe"))
                        {
                            proc = process;
                            flag = true;
                            break;
                        }
                    }
                    catch (System.ComponentModel.Win32Exception e)
                    {
                        //Console.WriteLine(e);

                    }
                }


                if (flag)
                {

                    Console.WriteLine($"Найден процесс с id = {proc.Id}");
                    var cLine = CommandLineUtilities.getCommandLines(proc);
                    Console.WriteLine($"comLine = {cLine}");
                    var arg = cLine.Split(" ");
                    Console.WriteLine($"arg = {String.Join(" ", arg)}");

                    foreach (string jsonArg in config.dll)
                    {
                        Console.WriteLine($"Kill = {jsonArg}");
                        if (arg.Any(x => x.Contains(jsonArg, StringComparison.OrdinalIgnoreCase)))
                        {
                            //proc.Kill(true);
                            KillProc(proc);
                        }
                        else
                        {
                            Console.WriteLine($"не подошел");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Нужного процесса не найдено");
                }




            }
            catch (Exception e)
            {
                Console.WriteLine(e);

            }
        }



        public static void KillProc(Process pocDotnet)
        {

            Console.WriteLine($"Будет удалён процесс с id = {pocDotnet.Id.ToString()}");
            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.Arguments = "/C " + "taskkill /f /t /pid " + pocDotnet.Id.ToString();
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.Start();


        }

        private static void Versua(ConfigDTO config, string[] args)
        {
            var context = File.ReadAllLines(args[0]);
            int[] versia = new int[4];
            int index = Convert.ToInt32(args[1]);
            if (!String.IsNullOrEmpty(config.version))
                versia = config.version.Split(".").Select(x => Convert.ToInt32(x)).ToArray();
            Console.WriteLine($"Version input: {String.Join(".", versia)}");
            versia[index]++;



            for (int i = 0; i < context.Length; i++)
                if (context[i].Contains("AssemblyVersion"))
                {

                    StringBuilder sb = new StringBuilder(100);

                    var indd = context[i].IndexOf("<");

                    string space = context[i].Substring(0, indd);
                    string verFile = $"{versia[0]}.{versia[1]}.{versia[2]}.{versia[3]}";

                    sb.Append(space);
                    sb.Append("<AssemblyVersion>");
                    sb.Append(verFile);
                    sb.Append("</AssemblyVersion>");

                    context[i] = sb.ToString();

                    File.WriteAllLines(args[0], context);

                    config.version = verFile;


                    Console.WriteLine($"Version output: {String.Join(".", versia)}");


                }


        }




        public int Concatent(int a, int b)
        {
            return a + b;
        }


    }




    public static class CommandLineUtilities
    {

        public static Process? GetParent(Process process)
        {
            try
            {
                using var query = new ManagementObjectSearcher(
                    $"SELECT * FROM Win32_Process WHERE ProcessId={process.Id}");
                return query
                    .Get()
                    .OfType<ManagementObject>()
                    .Select(p => Process.GetProcessById((int)(uint)p["ParentProcessId"]))
                    .FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

        public static String getCommandLines(Process processs)
        {
            ManagementObjectSearcher commandLineSearcher = new ManagementObjectSearcher(
                "SELECT CommandLine FROM Win32_Process WHERE ProcessId = " + processs.Id);
            String commandLine = "";
            foreach (ManagementObject commandLineObject in commandLineSearcher.Get())
            {
                commandLine += (String)commandLineObject["CommandLine"];
            }

            return commandLine;
        }

        public static String[] getCommandLinesParsed(Process process)
        {
            return (parseCommandLine(getCommandLines(process)));
        }

        /// <summary>
        /// Эта процедура преобразует командную строку в массив строк
        /// Нулевой элемент — имя программы
        /// Аргументы командной строки заполняют оставшуюся часть массива
        /// Во всех случаях значения удаляются из кавычек
        /// </summary>
        /// <param name="commandLine"></param>
        /// <returns>String array</returns>
        public static String[] parseCommandLine(String commandLine)
        {
            List<String> arguments = new List<String>();

            Boolean stringIsQuoted = false;
            String argString = "";
            for (int c = 0; c < commandLine.Length; c++)  //обработка строки по одному символу подряд
            {
                if (commandLine.Substring(c, 1) == "\"")
                {
                    if (stringIsQuoted)  //заканчиваем кавычку, поэтому заполняем следующий элемент списка построенным аргументом
                    {
                        arguments.Add(argString);
                        argString = "";
                    }
                    else
                    {
                        stringIsQuoted = true; //начало кавычки, поэтому флаг и пропуск
                    }
                }
                else if (commandLine.Substring(c, 1) == "".PadRight(1))
                {
                    if (stringIsQuoted)
                    {
                        argString += commandLine.Substring(c, 1); //пробел заключен в кавычки, поэтому сохраните его
                    }
                    else if (argString.Length > 0)
                    {
                        arguments.Add(argString);  //пустое поле без кавычек, поэтому добавьте его в список, если оно первое подряд пустое
                    }
                }
                else
                {
                    argString += commandLine.Substring(c, 1);  //неблановый символ: добавьте его в конструируемый элемент
                }
            }

            return arguments.ToArray();

        }

    }

}
